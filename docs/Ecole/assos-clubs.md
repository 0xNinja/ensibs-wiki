# 🎭 Associations et clubs

## Hack2G2 📚

Hack2g2 est une association créée par les étudiants de l'IUT de Vannes et de l'ENSIBS pour promouvoir le partage des connaissances majoritairement informatiques. Événements, conférences, lives et ateliers sont organisés par l'association pour les membres. Chaque année, l'association organise la hitchhack, une conférence de plusieurs jours avec de nombreux intervenants.

- [Discord](https://discord.gg/RSJJqE48Jw)
- [Site Web](https://hack2g2.fr/)
- [Twitter](https://twitter.com/Hack2G2)
- [Peertube](https://videos.hack2g2.fr/)

## Club CTF - APT56 🚩

Envie de soulever tous les CTF de France et de Navarre jusqu'au FCSC ? Alors bienvenue au club CTF.

## ENSIBF Holdings 💰

Vous souhaitez parler actions, cryptomonnaies et grosse moula ? Bienvenue chez ENSIBF Holding, le club des petits investisseurs rusés qui partagent leurs astuces et savoir faire.
